import numpy as np
import pandas as pd
from pandas.core.frame import DataFrame
from pandas.core.series import Series
from scipy.stats import norm
import functools
from sklearn.model_selection import train_test_split


def p_y(data, number, searching_atribute):
    x = data[searching_atribute].value_counts()
    x.to_dict()
    index = data.index
    number_of_rows = len(index)
    number_of_occurences = x[number]

    return number_of_occurences/number_of_rows


def get_probability(data, number, argument, value, searching_value):
    #data - dane
    # number - numer quality
    # argument - czyli co badamy np alkoihol
    # value - ile ten kos ma tego argumentu
    do_sprawdzenia = data[data[searching_value] == number][argument]
    mu, std = norm.fit(data[data[searching_value] == number][argument])
    if std == 0:
        return 1
    return (1/(std*np.sqrt(2*np.pi))*np.exp(-1/2*((value-mu)/std)**2))


def calculate_correctness(data_u, data_w, searching_attribute):
    index = data_w.index
    lendth_of_test_data = len(index)
    correctness_counter = 0
    error = 0
    list_of_collumns = []
    for column in data_u.columns:
        list_of_collumns.append(column)

    values = set(data_u[searching_attribute].values)
    dict_probability = {}
    for value in values:
        dict_probability[value] = p_y(data_u, value, searching_attribute)

    for index, row in data_w.iterrows():
        best_prob = 0
        best_val = 0
        for value in dict_probability:
            result = dict_probability[value]
            for column in data_w.columns:
                if column != searching_attribute:
                    result *= get_probability(data_u,
                                              value, column, row[column], searching_attribute)
            if best_prob < result:
                best_prob = result
                best_val = value
        if best_val == row[searching_attribute]:
            correctness_counter += 1
        else:
            error += abs(best_val-row[searching_attribute])
    return error/lendth_of_test_data


def k_validation(k, data, searching_attribute):
    z = np.array_split(data, k)
    result = []
    for i in range(k):
        test = z[i]
        train = data.drop(test.index)
        result.append(calculate_correctness(train, test, "quality"))
    return result


data = pd.read_csv("lab4/unprocessed_data/winequality-white.csv", sep=';')
train, test = train_test_split(data, test_size=.4)
# a = p_y(train, 9, "quality")
# b = get_probability(data, 8, 'alcohol', 0)

percentage_of_correctness1 = calculate_correctness(train, test, "quality")
percentage_of_correctness2 = k_validation(5, data.sample(frac=1), "quality")

print(percentage_of_correctness1)
print(percentage_of_correctness2)
print(sum(percentage_of_correctness2)/5)
