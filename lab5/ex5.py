import numpy as np
import pandas as pd


def sigmoid_function(x):
    return 1/(1 + np.exp(-x))


def sigmoid_function_derivative(x):
    return sigmoid_function(x) * (1 - sigmoid_function(x))


class NeuralNetwork:
    def __init__(self, number_of_input_neurons, number_of_output_neurons, number_of_hidden_layer_neurons, activation_function, activation_function_derivative, epochs) -> None:
        self.number_of_input_neurons = number_of_input_neurons
        self.number_of_output_neurons = number_of_output_neurons
        self.number_of_hidden_layer_neurons = number_of_hidden_layer_neurons
        self.activation_function = activation_function
        self.activation_function_derivative = activation_function_derivative
        self.weights_input_hidden = np.random.rand(
            self.number_of_hidden_layer_neurons, self.number_of_input_neurons)
        self.weights_hidden_output = np.random.rand(
            self.number_of_output_neurons, self.number_of_hidden_layer_neurons)
        self.biases1 = np.random.rand(self.number_of_hidden_layer_neurons, 1)
        self.biases2 = np.random.rand(self.number_of_output_neurons, 1)
        self.iterator = 0
        self.epochs = epochs

    def get_predictions(self, A2):
        return np.argmax(A2, 0)

    def get_accuracy(self, predictions, Y):
        Y = Y.astype(int) - 3
        print(predictions, Y)
        return np.sum(predictions == Y) / Y.size

    def run(self, alpha, X_train, Y_train):

        while self.iterator < self.epochs:
            Z1, A1, Z2, A2 = self.feed_forward(X_train)
            dW1, db1, dW2, db2 = self.back_propagation(
                Z1, A1, Z2, A2, X_train, Y_train)
            self.update_params(
                dW1, db1, dW2, db2, alpha)

            self.iterator += 1
            if not self.iterator % 10:
                print("Iteracja:", self.iterator)
                predictions = self.get_predictions(A2)
                print(self.get_accuracy(predictions, Y_train))

        return self.weights_input_hidden, self.biases1, self.weights_hidden_output, self.biases2

    def get_wine_data(self):
        return pd.read_csv("lab5/unprocessed_data/winequality-white.csv", sep=';')

    def softmax(self, Z):
        A = np.exp(Z) / sum(np.exp(Z))
        return A

    def feed_forward(self, X):
        """[summary]

        Args:
            X (list): input data
        Returns:
            Z1: sum of weights and input data and biases of hidden layer
            A1: Z1 with propagation from sigmoid function
            Z2: sum of weights and data and biases of output layer
            A2: Z2 with propagation from sigmoid function
        """
        Z1 = self.weights_input_hidden.dot(X) + self.biases1
        A1 = sigmoid_function(Z1)
        Z2 = self.weights_hidden_output.dot(A1) + self.biases2
        A2 = sigmoid_function(Z2)
        return Z1, A1, Z2, A2

    def input_vector(self, Y_train):
        """
        create array with zeros except correct quality value (index shifted by 3 to start from 0 not from 3(min quality==3))
        example:
        quality=7:
        single_array = [0,0,0,0,1,0,0].T
                 3,4,5,6,7,8,9

        array is create from multiple single arrays for every row of training data
        """

        array = np.zeros((7, Y_train.size))
        Y_train = Y_train.astype(int) - 3

        array[Y_train, np.arange(Y_train.size)] = 1
        return array

    def back_propagation(self, Z1, A1, Z2, A2, X, Y):
        """
        calculate deltas.
        A2 - output layer values
        Z2 - output layer values before sigmoid function
        A2 - hidden layer values
        Z2 - hidden layer values before sigmoid function
        """
        m = Y.size
        dZ2 = (A2 - self.input_vector(Y))**2
        dW2 = 1/m*2*dZ2.dot(A1.T)
        db2 = 1 / m * np.sum(dZ2)
        dZ1 = self.weights_hidden_output.T.dot(
            dZ2)*self.activation_function_derivative(Z1)
        dW1 = 1/m*dZ1.dot(X.T)
        db1 = 1/m*np.sum(dZ1)
        return dW1, db1, dW2, db2

    def update_params(self, dW1, db1, dW2, db2, alpha):
        """
        Update weights and biases
        """
        self.weights_input_hidden -= alpha * dW1
        self.biases1 -= alpha * db1
        self.weights_hidden_output -= alpha * dW2
        self.biases2 -= alpha * db2


if __name__ == "__main__":
    # nmb of  input,nmb of  hidden,nmb of layer, function, derivative, epochs
    n1 = NeuralNetwork(11, 7, 16, sigmoid_function,
                       sigmoid_function_derivative, 10000)
    data = pd.read_csv("lab5/unprocessed_data/winequality-white.csv", sep=';')
    data = np.array(data)
    m, n = data.shape
    np.random.shuffle(data)

    data_dev = data[0:100].T
    Y_dev = data_dev[11]
    X_dev = data[0:11]

    # rows from 100 to end
    data_train = data[100:m].T

    # quality
    Y_train = data_train[11]

    # data except quality
    X_train = data_train[0:11]
    print(X_train)

    n1.run(0.001, X_train, Y_train)
"""Ćw 5. (8 pkt), data oddania: do 03.01.2022 - Sztuczne sieci neuronowe
Zaimplementować sztuczną sieć neuronową z warstwą ukrytą.
Implementacja powinna być elastyczna na tyle, żeby można było zdefiniować liczbę neuronów wejściowych, ukrytych i wyjściowych.
Wykorzystać sigmoidalną funkcję aktywacji i do trenowania użyć wstecznej propagacji błędu z użyciem metody stochastycznego najszybszego spadku.
Sieć nauczyć rozpoznawać jakość wina jak w ćwiczeniu 4. i porównać wyniki z otrzymanymi poprzednio.
Na wykresie pokazać jak zmieniał się błąd uczonej sieci w kolejnych epokach.
Poeksperymentować ze współczynnikiem uczenia oraz liczbą epok.
"""
