import numpy as np
import random

map = """...#############
...#..S..#..#..#
#..####..#..#..#
#........#.....#
#..#..#..#..#..#
#..#..#.....#..#
#..####..#..####
#..#.F#..#.....#
#..#..#######..#
#.....#.........
#############..."""
rows = [i for i in map.split("\n")]
print(len(rows))

costs = np.zeros([len(rows), len(rows[0])])
number_of_rows = len(rows)
number_of_columns = len(rows[0])


def get_column(i):
    column = ""
    for k in range(number_of_rows):
        column += rows[k][i]
    return column


map_table = [get_column(column) for column in range(number_of_columns)]


def get_next_state(direction_number, current_state):
    x_value = 1 if direction_number == 1 else -1 if direction_number == 3 else 0
    y_value = 1 if direction_number == 2 else -1 if direction_number == 0 else 0
    if current_state[0] == 0 and x_value == -1:
        return current_state
    if current_state[1] == 0 and y_value == -1:
        return current_state
    if current_state[0] == number_of_columns-1 and x_value == 1:
        return current_state
    if current_state[1] == number_of_rows-1 and y_value == 1:
        return current_state
    return (current_state[0] + x_value, current_state[1] + y_value)


def take_random_direction():
    option = random.randint(0, 3)
    return option


best_path = []


def print_path(prowizoryczne_koszty, start, end):
    print("pole:", start)
    best_path.append(start)
    if start == (end[0], end[1]):
        return
    maximum = max(prowizoryczne_koszty[start[0]][start[1]])
    for i in range(4):
        if maximum == prowizoryczne_koszty[start[0]][start[1]][i]:

            return print_path(prowizoryczne_koszty, get_next_state(i, start), end)


def get_best_option(prowizoryczne_koszty, start):
    maximum = max(prowizoryczne_koszty[start[0]][start[1]])
    for i in range(4):
        if maximum == prowizoryczne_koszty[start[0]][start[1]][i]:
            return i


def get_penalty(state, map_table):
    char = map_table[state[0]][state[1]]

    if char == "#":
        return -10
    if char == "F":
        return 1
    else:
        return -1


prowizoryczne_koszty = []
for x in range(number_of_columns):
    prowizoryczne_koszty.append([])
    for y in range(number_of_rows):
        prowizoryczne_koszty[x].append([])
        for option in range(4):
            prowizoryczne_koszty[x][y].append(0)


def get_start_state(map_table):
    for x in range(number_of_columns):
        for y in range(number_of_rows):
            if map_table[x][y] == "S":
                return (x, y)


def get_end_state(map_table):
    for x in range(number_of_columns):
        for y in range(number_of_rows):
            if map_table[x][y] == "F":
                return (x, y)


liczba_krokow_list = []
beta = 0.01
epsilon = 0.01
gamma = 0.01
penalty_list = []

for i in range(10000):
    done = False
    state = get_start_state(map_table)
    penalty_sum = 0
    liczba_krokow = 0
    while not done:
        penalty = get_penalty(state, map_table)
        if i % 10 == 1:
            liczba_krokow += 1
            penalty_sum += penalty

        # strategia epsilon zachłanna
        if random.uniform(0, 1) < epsilon:
            option = take_random_direction()
        else:
            option = get_best_option(prowizoryczne_koszty, state)
        next_state = get_next_state(option, state)

        prowizoryczne_koszty[state[0]][state[1]][option] = prowizoryczne_koszty[state[0]][state[1]][option]*(
            1 - beta) + beta*(penalty + gamma*max(prowizoryczne_koszty[next_state[0]][next_state[1]]))
        if map_table[next_state[0]][next_state[1]] == 'F':
            done = True
            if i % 10 == 1:
                penalty_list.append(penalty_sum)
                liczba_krokow_list.append(liczba_krokow)

        else:
            state = next_state
start = get_start_state(map_table)
print_path(prowizoryczne_koszty,
           (start[0], start[1]), get_end_state(map_table))
print("Best path", best_path)
print(penalty_list)
print(liczba_krokow_list)
