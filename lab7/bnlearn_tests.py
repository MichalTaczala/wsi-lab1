import bnlearn as bn
from pgmpy.factors.discrete import TabularCPD

# burg
studia_inf = TabularCPD(variable='studia_inf',
                        variable_card=2, values=[[0.9], [0.1]])
lubienie_gier = TabularCPD(variable='lubienie_gier',
                           variable_card=2, values=[[0.4], [0.6]])
duzo_przed_komputerem = TabularCPD(variable='duzo_przed_komputerem', variable_card=2,
                                   values=[[0.9, 0.5, 0.3, 0.1],
                                           [0.1, 0.5, 0.7, 0.9]],
                                   evidence=['studia_inf', 'lubienie_gier'],
                                   evidence_card=[2, 2])
popsucie_wzroku = TabularCPD(variable='popsucie_wzroku', variable_card=2,
                             values=[[0.9, 0.3],
                                     [0.1, 0.7]],
                             evidence=['duzo_przed_komputerem'], evidence_card=[2])


edges = [('studia_inf', 'duzo_przed_komputerem'),
         ('lubienie_gier', 'duzo_przed_komputerem'),
         ('duzo_przed_komputerem', 'popsucie_wzroku')]

DAG = bn.make_DAG(edges)
DAG = bn.make_DAG(DAG, CPD=[studia_inf, lubienie_gier,
                  duzo_przed_komputerem, popsucie_wzroku])

# bn.print_CPD(DAG)


bn.inference.fit(DAG, variables=['studia_inf'], evidence={})


bn.inference.fit(DAG, variables=['popsucie_wzroku'], evidence={})

bn.inference.fit(DAG, variables=['popsucie_wzroku'], evidence={
                 'studia_inf': 1})

bn.inference.fit(DAG, variables=['duzo_przed_komputerem'], evidence={
                 'lubienie_gier': 1})

bn.inference.fit(DAG, variables=['popsucie_wzroku'], evidence={
                 'lubienie_gier': 0})

bn.inference.fit(DAG, variables=['popsucie_wzroku'], evidence={
                 'studia_inf': 0, 'duzo_przed_komputerem': 0})
