import random


def generate_samples(number_of_samples, relations):
    samples = []
    for _ in range(number_of_samples):
        samples.append(generate_single_sample(relations))
    return samples


def generate_single_sample(relations):
    sample = []
    for level in relations:
        for element in level:
            element_number = element[0]
            name = element[1]
            probability = get_element_probability(sample, element)
            is_true = True if random.random() <= probability else False
            sample.append((element_number, name, is_true))

    return sample


def get_element_probability(sample, element):
    if type(element[2]) == float:
        return element[2]
    element_number = element[0]
    previous_calculated_states = {}
    for option in sample:
        number = option[0]
        is_true = option[2]
        previous_calculated_states[number] = is_true
    for connections_with_previous_states in element[2]:
        values_dict = {}

        numbers = connections_with_previous_states[0].split(" ")
        for number in range(1, element_number):
            if str(number) in element[3]:
                if str(number) in numbers:
                    values_dict[number] = True
                else:
                    values_dict[number] = False
        if values_dict.items() <= previous_calculated_states.items():
            return connections_with_previous_states[1]


def calculate_samples_rate(samples, name, value, confirmed_attribute_values):
    counter = 0
    number_of_correct_samples = 0
    required_names = [name[0] for name in confirmed_attribute_values]
    values_dict = {name[0]: name[1] for name in confirmed_attribute_values}
    required_counter = len(required_names)
    for sample in samples:
        required_counter = len(required_names)

        for attribute in sample:
            if attribute[1] in required_names and attribute[2] == values_dict[attribute[1]]:
                required_counter -= 1
            if required_counter == 0 and attribute[1] == name:
                number_of_correct_samples += 1
                if attribute[2] == value:
                    counter += 1
    return counter/number_of_correct_samples


if __name__ == "__main__":

    number_of_samples = 100
    """relations = list of levels in our graph.
    in every level we have values:(number, name, probability) if it is not dependent on other states.
    if it is, we have(number, name, list with(True values, probability for that option) )"""
    relations = [[(1, "studia na kierunku informatycznym", 0.1), (2, "lubienie grać w gry", 0.6)],
                 [(3, "dużo czasu spędzanego przed komputerem", [("1 2", 0.9),
                                                                 ("1", 0.7), ("2", 0.5), ("", 0.1)], ("1", "2"))],
                 [(4, "pogorszenie wzroku", [("3", 0.7), ("", 0.1)], "3")]]

    samples = generate_samples(number_of_samples, relations)

    b = calculate_samples_rate(
        samples, "pogorszenie wzroku", True, [("studia na kierunku informatycznym", False), ("dużo czasu spędzanego przed komputerem", False)])
    print(b)
