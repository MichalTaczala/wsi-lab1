import matplotlib.pyplot as plt
import numpy as np


def f2(x):
    return x**2 + 3*x - 8


def f1(x):
    return x**4 - 5*(x**2) - 3*x


def get_derivative1(x):
    return 4*(x**3) - 10*x - 3


def get_derivative2(x):
    return 2*x+3


def calculate(x0, alpha, number_of_function, max_iterations=0, plot=False):
    is_done = False
    x = x0
    iterations = 0
    while not is_done:

        if number_of_function == 1:
            delta = get_derivative1(x)
            function = f1
        elif number_of_function == 2:
            delta = get_derivative2(x)
            function = f2
        if plot:
            if iterations == 0:
                plt.plot(x, function(x), 'og')
            else:
                plt.plot(x, function(x), 'c*')
        x_prev = x
        x -= alpha*delta
        iterations += 1

        if x_prev == x or (iterations > max_iterations and max_iterations != 0):
            print(f"liczba iteracji funkcji 1 dla alpha={alpha}:", iterations)
            if plot:
                plt.plot(x, function(x), 'or')
                if not max_iterations:
                    # edit params below to adjust text
                    plt.text(x-0.7, function(x)-2, f"Iteracje: {iterations}")
            is_done = True
    return x


if __name__ == "__main__":
    x0 = -1.49
    alpha = 1

    t = np.linspace(-6, 3, 100)

    # edit line below to change functions
    number_of_function = 2

    # edit line below to switch funcions and plot's x
    plt.plot(t, [f2(t) for t in t])

    plt.title(
        f'Funckcja {number_of_function})\n alpha = {alpha}\nx0 = {x0}\n ')

    plt.xlabel('x')
    plt.ylabel('y')

    calculate(x0, alpha, number_of_function, 10000, True)
    plt.show()
