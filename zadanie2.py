import numpy as np
import random
import matplotlib.pyplot as plt
import numpy as np
from numpy import sin
from numpy import *
from numpy import pi
from numpy import sqrt
from matplotlib import cm
from numpy.random.mtrand import rand
import time


def f(x) -> int:
    return x**2


def rosenbrock_f(arguments):
    x1 = arguments[0]
    x2 = arguments[1]
    return (1-x1)**2 + 100*(x2 - x1**2)**2


def shubert_f(arguments):
    x1 = arguments[0]
    x2 = arguments[1]
    sum1 = 0
    sum2 = 0
    for i in range(1, 6):
        sum1 = sum1 + (i * cos(((i+1)*x1) + i))
        sum2 = sum2 + (i * cos(((i+1)*x2) + i))
    return sum1 * sum2


def bird_f(arguments):
    x1 = arguments[0]
    x2 = arguments[1]
    return np.sin(x1)*(np.exp(1-np.cos(x2))**2)+np.cos(x2)*(np.exp(1-np.sin(x1))**2)+(x1-x2)**2


def f2(x):
    return x**8-90*x**6+13*x**5+19*x**4-4*x**2+9*x-12438


def find_best(P0, func):
    y_min = float("inf")
    x_min = None
    index = None
    for i, element in enumerate(P0):
        if func(element) < y_min:
            y_min = func(element)
            index = i
            x_min = element
    return x_min, y_min


def get_mininum_from_list(population, function):
    minimum = float("inf")
    x_min = None
    for element in population:
        if function(element) < minimum:
            minimum = function(element)
            x_min = element
    return x_min


def reproduction(population, size_of_tournament, f):
    population_size = len(population)

    result = []
    for round in range(population_size):
        participants = []
        for _participant in range(size_of_tournament):
            index = random.randint(0, len(population)-1)
            participant = population[index]
            participants.append(participant)
        x = get_mininum_from_list(participants, f)
        result.append(x)
    return result


def mutation(x, sigma):
    tmp = []
    for i in range(len(x)):
        tmp.append(x[i] + sigma*np.random.normal(0, 1))
    return tmp


def succession(Pt, M, function, k):
    # PT+1 = KNAJLEPSZYCH Z PT+ M - KNAJGORZSZYCH Z NICH
    Pt_new = []
    list_of_tuples = [(x, function(x)) for x in Pt]
    sorted_list = sorted(list_of_tuples, key=lambda item: item[1])
    Pt_new += sorted_list[:k]
    Pt_new += [(x, function(x)) for x in M]
    sorted_list1 = sorted(Pt_new, key=lambda item: item[1])
    final_list = sorted_list1[:-k]
    final_list = [x for x, y in final_list]
    return final_list.copy()


def crossing(R, pc, alpha):
    number_of_iterations = len(R)//2
    for i in range(number_of_iterations):
        number = random.uniform(0, 1)

        if number <= pc:
            tmp1 = []
            tmp2 = []
            for k in range(len(R[0])):
                child1_arg = R[2*i+1][k]*alpha + R[2*i][k]*(1 - alpha)
                tmp1.append(child1_arg)
                child2_arg = R[2*i + 1][k]*(1-alpha) + R[2*i][k]*alpha
                tmp2.append(child2_arg)
            child1 = tmp1.copy()
            child2 = tmp2.copy()

            R[2*i + 1] = child1
            R[2*i] = child2
    return R


def genetic_operations(R, sigma, pc, pm, alpha):
    number = random.uniform(0, 1)
    R = crossing(R, pc, alpha)
    for i, x in enumerate(R):
        if number <= pm:
            new_x = mutation(x, sigma)
            R[i] = new_x
    return R


def get_random_population(population_size, dimensions=1,
                          down_limit=-6, upper_limit=6, type_=0):

    result = []
    if not type_:
        for _ in range(population_size):
            temporary = []
            for _ in range(dimensions):
                random_number = random.uniform(down_limit, upper_limit)
                temporary.append(random_number)
            result.append(temporary)
    else:
        iters = population_size//type_
        for i in range(iters):
            temporary = []
            for _ in range(dimensions):
                number = random.uniform(down_limit, upper_limit)
                temporary.append(number)
            for k in range(type_):
                result.append(temporary)
        for _ in range(population_size - type_*iters):
            temporary = []
            for _ in range(dimensions):
                random_number = random.uniform(down_limit, upper_limit)
                temporary.append(random_number)
            result.append(temporary)

    return result


def find_optimum(q, P0, sigma, pc, max_iterations, pm, size_of_tournament, k, alpha, plot_option):
    iterations = 0
    x_r, y_r = find_best(P0, q)
    Pt = P0.copy()
    while iterations < max_iterations:
        R = reproduction(Pt, size_of_tournament, q)
        M = genetic_operations(R, sigma, pc, pm, alpha)
        x_t, y_t = find_best(M, q)

        if y_t < y_r:
            y_r = y_t
            x_r = x_t
      
        Pt = succession(Pt, M, q, k)
        iterations += 1
   
    return x_r, y_r


def gradient_plot(_function, down_limit, upper_limit):
    x = np.linspace(down_limit-3, upper_limit+3, 100)
    y = np.linspace(down_limit-3, upper_limit+3, 100)
    X, Y = np.meshgrid(x, y)
    Z = _function([X, Y])
    plt.contourf(X, Y, Z, cmap='RdGy')
    plt.colorbar()


def result_on_iteration_plot():
    _function = bird_f
    max_iterations = 200
    dimensions = 2

    find_optimum(_function, get_random_population(population_size=10, dimensions=2, down_limit=-6, upper_limit=6),
                 0.1, 0.8, max_iterations, pm=0.01, size_of_tournament=2, k=4, alpha=0.1, plot_option=0)
    find_optimum(_function, get_random_population(population_size=10, dimensions=2, down_limit=-6, upper_limit=6),
                 1, 0.8, max_iterations, pm=0.1, size_of_tournament=4, k=8, alpha=0.1, plot_option=0)
    find_optimum(_function, get_random_population(population_size=10, dimensions=2, down_limit=-6, upper_limit=6),
                 2, 0.8, max_iterations, pm=0.2, size_of_tournament=2, k=1, alpha=0.1, plot_option=0)
    find_optimum(_function, get_random_population(population_size=10, dimensions=2, down_limit=-6, upper_limit=6),
                 3, 0.8, max_iterations, pm=0.4, size_of_tournament=2, k=10, alpha=0.1, plot_option=0)
    plt.legend([f"Sigma:0.1 Rozmiar elity:4 Prawdopodobieństwo mutacji 0.01", f"Sigma:1 Rozmiar elity:8 Prawdopodobieństwo mutacji 0.1",
               f"Sigma:2 Rozmiar elity:1 Prawdopodobieństwo mutacji 0.2", f"Sigma:3 Rozmiar elity:10 Prawdopodobieństwo mutacji 0.4"])
    plt.xlabel("Liczba iteracji")
    plt.ylabel("Wartość rozwiązania")
    # find_optimum(_function, get_random_population(population_size=1000, dimensions=2, down_limit=-6, upper_limit=6),
    #              1, 0.8, max_iterations, pm=0.4, size_of_tournament=4, k=2, alpha=0.1, plot_option=0)
    # find_optimum(_function, get_random_population(population_size=100, dimensions=2, down_limit=-6, upper_limit=6),
    #              1.5, 0.8, max_iterations, pm=0.6, size_of_tournament=6, k=1, alpha=0.1, plot_option=0)


if __name__ == '__main__':
    max_iterations = 100
    size_of_populations_init = 20  # 10,100, 500
    size_of_tournament = 2  # 2,4 itp
    size_of_elite = 2  # 1, 10, 20 itp
    type_ = 0
    sigma = 1  # sila mutacji
    mutation_probability = 0.1  # prawdopodobienstwo mutacji

    alpha = 0.1
    pc = 1

    _function = bird_f
    down_limit = -6
    upper_limit = 6
    plot_option = 0
    dimensions = 1 if _function == f else 2

    if plot_option == 0:  # wyniki na iteracje
        result_on_iteration_plot()
    if plot_option == 1:  # gradient
        gradient_plot(_function, down_limit, upper_limit)
        op = find_optimum(_function, get_random_population(size_of_populations_init, dimensions, down_limit, upper_limit, type_),
                          sigma, pc, max_iterations, mutation_probability, size_of_tournament, size_of_elite, alpha, plot_option)

    if plot_option == 2:
        # results = []
        # start = time.time()
        # op = find_optimum(_function, get_random_population(4, dimensions, down_limit, upper_limit, type_),
        #                   sigma, pc, max_iterations, mutation_probability, size_of_tournament, size_of_elite, alpha, plot_option)
        # results.append((4, time.time() - start))
        # start = time.time()
        # op = find_optimum(_function, get_random_population(10, dimensions, down_limit, upper_limit, type_),
        #                   sigma, pc, max_iterations, mutation_probability, size_of_tournament, size_of_elite, alpha, plot_option)
        # results.append((50, time.time() - start))
        # start = time.time()
        # op = find_optimum(_function, get_random_population(100, dimensions, down_limit, upper_limit, type_),
        #                   sigma, pc, max_iterations, mutation_probability, size_of_tournament, size_of_elite, alpha, plot_option)
        # results.append((100, time.time() - start))
        # start = time.time()
        # op = find_optimum(_function, get_random_population(500, dimensions, down_limit, upper_limit, type_),
        #                   sigma, pc, max_iterations, mutation_probability, size_of_tournament, size_of_elite, alpha, plot_option)
        # results.append((500, time.time() - start))
        # start = time.time()
        # op = find_optimum(_function, get_random_population(750, dimensions, down_limit, upper_limit, type_),
        #                   sigma, pc, max_iterations, mutation_probability, size_of_tournament, size_of_elite, alpha, plot_option)
        # results.append((750, time.time() - start))
        # start = time.time()
        # op = find_optimum(_function, get_random_population(1000, dimensions, down_limit, upper_limit, type_),
        #                   sigma, pc, max_iterations, mutation_probability, size_of_tournament, size_of_elite, alpha, plot_option)
        # results.append((1000, time.time() - start))
        # start = time.time()
        # op = find_optimum(_function, get_random_population(5000, dimensions, down_limit, upper_limit, type_),
        #                   sigma, pc, max_iterations, mutation_probability, size_of_tournament, size_of_elite, alpha, plot_option)
        # results.append((5000, time.time() - start))
        # pop_size = [p for p, z in results]
        # c = [z for p, z in results]
        # plt.plot(pop_size, c)
        # plt.xscale("log")
        # plt.xlabel("Rozmiar populacji")
        # plt.ylabel("Liczba sekund na wykonanie się algorytmu")
        results = [[], [], [], [], [], [], []]
        for _ in range(3):
            op = find_optimum(_function, get_random_population(4, dimensions, down_limit, upper_limit, type_),
                              sigma, pc, max_iterations, mutation_probability, size_of_tournament, size_of_elite, alpha, plot_option)
            results[0].append(op[1])
            op = find_optimum(_function, get_random_population(10, dimensions, down_limit, upper_limit, type_),
                              sigma, pc, max_iterations, mutation_probability, size_of_tournament, size_of_elite, alpha, plot_option)
            results[1].append(op[1])
            op = find_optimum(_function, get_random_population(100, dimensions, down_limit, upper_limit, type_),
                              sigma, pc, max_iterations, mutation_probability, size_of_tournament, size_of_elite, alpha, plot_option)
            results[2].append(op[1])
            op = find_optimum(_function, get_random_population(500, dimensions, down_limit, upper_limit, type_),
                              sigma, pc, max_iterations, mutation_probability, size_of_tournament, size_of_elite, alpha, plot_option)
            results[3].append(op[1])
            op = find_optimum(_function, get_random_population(750, dimensions, down_limit, upper_limit, type_),
                              sigma, pc, max_iterations, mutation_probability, size_of_tournament, size_of_elite, alpha, plot_option)
            results[4].append(op[1])
            op = find_optimum(_function, get_random_population(1000, dimensions, down_limit, upper_limit, type_),
                              sigma, pc, max_iterations, mutation_probability, size_of_tournament, size_of_elite, alpha, plot_option)
            results[5].append(op[1])
            op = find_optimum(_function, get_random_population(5000, dimensions, down_limit, upper_limit, type_),
                              sigma, pc, max_iterations, mutation_probability, size_of_tournament, size_of_elite, alpha, plot_option)
            results[6].append(op[1])
        for i, element in enumerate(results):
            sum_ = 0
            for k in element:
                sum_ += k
            sum_ /= len(element)
            results[i] = sum_
        pop_size = [4, 10, 100, 500, 750, 1000, 5000]
        c = [y for y in results]
        plt.plot(pop_size, c)
        plt.xscale("log")

        plt.xlabel("Rozmiar populacji")
        # plt.ylabel("Liczba sekund na wykonanie się algorytmu")
        plt.ylabel("Średni wynik działania algorytmu")

    plt.ioff()
    plt.show()
