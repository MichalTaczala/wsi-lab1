from typing import Collection
import copy
import random


class TicTacToe:

    def __init__(self):
        self.board = [["", "", ""], ["", "", ""], ["", "", ""]]
        self.result = {"X": 1,
                       "O": -1,
                       "TIE": 0,
                       "": 0}
        self.best_move = (0, 0)
        self.iterations = 0

    def minimax_with_ab(self, board_n, depth, alpha, beta, maximizing_player):
        game_won = self.check_winner(board_n)
        self.iterations += 1
        best_move = (0, 0)
        if depth == 0 or game_won:
            return self.result[game_won], best_move
        if maximizing_player:
            max_eval = -float("inf")
            # for child in self.board:
            for column in range(3):
                for row in range(3):
                    if board_n[column][row] == "":
                        board_n[column][row] = "X"
                        eval, _ = self.minimax_with_ab(
                            board_n, depth - 1, alpha, beta, False)
                        if max_eval < eval:
                            best_move = (column, row)
                        alpha = max(alpha, eval)
                        board_n[column][row] = ""
                        max_eval = max(max_eval, eval)

                        if beta <= alpha:
                            break

            return max_eval, best_move
        else:
            min_eval = float("inf")
            for column in range(3):
                for row in range(3):
                    if board_n[column][row] == "":
                        board_n[column][row] = "O"
                        eval, _ = self.minimax_with_ab(
                            board_n, depth - 1, alpha, beta, True)
                        if min_eval > eval:
                            best_move = (column, row)
                        beta = min(beta, eval)
                        board_n[column][row] = ""
                        if beta <= alpha:
                            break
                        min_eval = min(min_eval, eval)

            return min_eval, best_move

    def check_winner(self, board_n):
        points = 0

        for i in range(2):
            if i == 0:
                player = 'X'
            else:
                player = 'O'
            for column in range(3):
                for row in range(3):
                    if board_n[column][row] == player:
                        points += 1

                if points == 3:
                    return player
                points = 0
            points = 0
            for row in range(3):
                for column in range(3):
                    if board_n[column][row] == player:
                        points += 1

                if points == 3:
                    return player
                points = 0
            points = 0
            for k in range(3):
                if board_n[k][k] == player:
                    points += 1
            if points == 3:
                return player
            points = 0
            for k in range(3):
                if board_n[k][2 - k] == player:
                    points += 1
            if points == 3:
                return player
            points = 0
        for column in range(3):
            for row in range(3):
                if board_n[column][row] == "":
                    return ""
        points = 0
        return "TIE"

    def minimax(self, board_n, depth, maximizing_player):
        self.iterations += 1
        game_won = self.check_winner(board_n)

        best_move = (0, 0)
        if depth == 0 or game_won:
            return self.result[game_won], best_move
        if maximizing_player:
            max_eval = -float("inf")
            # for child in self.board:
            for column in range(3):
                for row in range(3):
                    if board_n[column][row] == "":
                        board_n[column][row] = "X"
                        eval, _ = self.minimax(board_n, depth - 1, False)
                        if max_eval < eval:
                            best_move = (column, row)
                        max_eval = max(max_eval, eval)
                        board_n[column][row] = ""

            return max_eval, best_move
        else:
            min_eval = float("inf")
            for column in range(3):
                for row in range(3):
                    if board_n[column][row] == "":
                        board_n[column][row] = "O"
                        eval, _ = self.minimax(board_n, depth - 1, True)
                        if min_eval > eval:
                            best_move = (column, row)
                        min_eval = min(min_eval, eval)

                        board_n[column][row] = ""
            return min_eval, best_move

    def random(self, board_n):
        positions = []
        for column in range(3):
            for row in range(3):
                if board_n[column][row] == "":
                    positions.append((column, row))
        result = random.choice(positions)
        return ("", result)

    def run_with_ab(self, depth, alpha, beta, max_player):

        while not self.check_winner(self.board):
            board_n = [["", "", ""], ["", "", ""], ["", "", ""]]
            a = ""
            for column in range(3):
                for row in range(3):
                    a = self.board[column][row]
                    board_n[column][row] = a

            if max_player:
                _, best_move = self.minimax(board_n, depth, max_player)
                self.board[best_move[0]][best_move[1]] = "X"
                if self.check_winner(self.board):
                    break
                # for column in range(3):
                #     for row in range(3):

                #         print(self.board[column][row], end="") if self.board[column][row] != "" else print(
                #             " ", end="")
                #     print("\n")
                answer = input("Wpisz kolumne, rzad: np(33)")

                column = int(answer[0])
                row = int(answer[1])
                self.board[column][row] = "O"

            else:
                _, best_move = self.minimax(board_n, depth, max_player)
                answer = input("Wpisz kolumne, rzad: np(33)")
                answer[1] = column
                answer[0] = row
                self.board[column][row] = "X"
        print(self.check_winner(self.board))

    def run(self, depth, max_player):

        while not self.check_winner(self.board):
            board_n = [["", "", ""], ["", "", ""], ["", "", ""]]
            a = ""
            for column in range(3):
                for row in range(3):
                    a = self.board[column][row]
                    board_n[column][row] = a

            if max_player:
                _, best_move = self.minimax(board_n, depth, max_player)

                self.board[best_move[0]][best_move[1]] = "X"
                if self.check_winner(self.board):
                    break
                # for column in range(3):
                #     for row in range(3):

                #         print(self.board[column][row], end="") if self.board[column][row] != "" else print(
                #             " ", end="")
                #     print("\n")
                answer = input("Wpisz kolumne, rzad: np(33)")

                column = int(answer[0])
                row = int(answer[1])
                self.board[column][row] = "O"

            else:
                _, best_move = self.minimax(board_n, depth, max_player)
                answer = input("Wpisz kolumne, rzad: np(33)")
                answer[1] = column
                answer[0] = row
                self.board[column][row] = "X"
        print(self.check_winner(self.board))

    def run_ai_vs_ai(self, depth1, depth2, player1, player2):
        max_player = True
        # if alfa_beta:
        #     alpha = -float("inf")
        #     beta = float("inf")
        while not self.check_winner(self.board):
            print(hex(id(self.board)))
            board_n = [["", "", ""], ["", "", ""], ["", "", ""]]
            a = ""
            for column in range(3):
                for row in range(3):
                    a = self.board[column][row]
                    board_n[column][row] = a

            if max_player:
                self.iterations = 0

                if player1 == "ab":
                    _, best_move = self.minimax_with_ab(
                        board_n, depth1, alpha, beta,  max_player)
                if player1 == "n":
                    _, best_move = self.minimax(board_n, depth1, max_player)
                if player1 == "r":
                    _, best_move = self.random(board_n)
                print(self.iterations)
                self.iterations = 0
                self.board[best_move[0]][best_move[1]] = "X"
                if self.check_winner(self.board):
                    break
                # for column in range(3):
                #     for row in range(3):

                #         print(self.board[column][row], end="") if self.board[column][row] != "" else print(
                #             " ", end="")
                #     print("\n")
                # answer = input("Wpisz kolumne, rzad: np(33)")

                # column = int(answer[0])
                # row = int(answer[1])
                # self.board[column][row] = "O"
                board_n = [["", "", ""], ["", "", ""], ["", "", ""]]
                a = ""
                for column in range(3):
                    for row in range(3):
                        a = self.board[column][row]
                        board_n[column][row] = a
                if player2 == "ab":
                    _, best_move = self.minimax_with_ab(
                        board_n, depth2, alpha, beta,  False)
                if player2 == "n":
                    _, best_move = self.minimax(board_n, depth2, False)
                if player2 == "r":
                    _, best_move = self.random(board_n)
                self.board[best_move[0]][best_move[1]] = "O"
                if self.check_winner(self.board):
                    break
                # for column in range(3):
                #     for row in range(3):

                #         print(self.board[column][row], end="") if self.board[column][row] != "" else print(
                #             " ", end="")
                #     print("\n")

        print(self.check_winner(self.board))


state = ""
depth = 2
depth1 = 9
depth2 = 3
max_player = True
alpha = -float("inf")
beta = float("inf")
t = TicTacToe()
# t.run(depth, max_player) //we vs ai
#t.run_with_ab(depth, max_player)
# n- normal
#ab - ab
# r random
t.run_ai_vs_ai(depth1, depth2, "ab", "ab")
#t.run(2, True)
